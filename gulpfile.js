
// **************** //
//       v1.2       //
//     9.07.17      //
// **************** //
// Объявление пакетов
var gulp 			= require('gulp'),
	sass 			= require('gulp-sass'),
	csso 			= require('gulp-csso'),
	rename 			= require('gulp-rename'),
	uglify 			= require('gulp-uglify'),
	browserSync  	= require('browser-sync'),
	imagemin 		= require('gulp-imagemin'),
	autoprefixer 	= require('gulp-autoprefixer');
	injectPartials 	= require('gulp-inject-partials'),
	reload 			= browserSync.reload;

// Пути до проета
var project = {
	main: 		'app/_project/',
	html: 		'app/_project/*.html',
	htmlAll: 	'app/_project/**/*.html',
	img: 		'app/_project/assets/img/**/*',
	js:   		'app/_project/assets/js/**/*.js',
	fonts: 		'app/_project/assets/fonts/**/*',
	libs: 		'app/_project/assets/libs/**/*',
	sass: 		'app/_project/assets/sass/**/*.scss',
};

// Пути до сборки
var build = {
	main: 		'app/build/',
	img:   		'app/build/assets/img/',
	css:  		'app/build/assets/css/',
	js:   		'app/build/assets/js/',
	fonts: 		'app/build/assets/fonts/',
	libs: 		'app/build/assets/libs/'
};

// Path HTML
gulp.task('injectPartials', function () {
  return gulp.src(project.html)
		.pipe(injectPartials())
		.pipe(gulp.dest(build.main));
});

// HTML
gulp.task('html', function(){
	gulp.src(project.htmlAll)
	.pipe(reload({stream:true}));
});

// SASS
gulp.task('sass', function(){
	gulp.src(project.sass)
	.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
	.pipe(autoprefixer())
	.pipe(gulp.dest(build.css))
	.pipe(reload({stream:true}))
	.pipe(rename(function (path) {
		path.basename += "-min";
	}))
	.pipe(csso())
	.pipe(gulp.dest(build.css));
});

// JS
gulp.task('js', function(){
	gulp.src(project.js)
	.pipe(gulp.dest(build.js))
	.pipe(uglify())
	.pipe(rename(function (path) {
		path.basename += "-min";
	}))
	.pipe(gulp.dest(build.js))
	.pipe(reload({stream:true}));
});

// IMAGE
gulp.task('img', function(){
	gulp.src(project.img)
	.pipe(imagemin())
	.pipe(gulp.dest(build.img))
	.pipe(reload({stream:true}));
});

// FONT
gulp.task('fonts', function(){
	gulp.src(project.fonts)
	.pipe(gulp.dest(build.fonts))
});

// LIBS
gulp.task('libs', function(){
	gulp.src(project.libs)
	.pipe(gulp.dest(build.libs))
});

// LIVERELOAD
gulp.task('browserSync', function() {
  browserSync({
	server: {
	  baseDir: build.main
	},
	port: 8080,
	open: true,
	notify: false
  });
});

// WATCH
gulp.task('watch',function(){
  gulp.watch(project.htmlAll, ['injectPartials', 'html']);
  gulp.watch(project.sass, ['sass']);
  gulp.watch(project.img, ['img']);
  gulp.watch(project.js, ['js']);
});

gulp.task('default', ['injectPartials','sass','js','img','watch','browserSync']);